<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Prueba\Application\Services\Create\CreateRideServiceApplication;
use Prueba\Application\Services\Create\CreateRideServiceApplicationInterface;
use Prueba\Application\Services\Get\GetRideServiceApplication;
use Prueba\Application\Services\Get\GetRideServiceApplicationInterface;
use Prueba\Application\Services\Update\UpdateRideServiceApplication;
use Prueba\Application\Services\Update\UpdateRideServiceApplicationInterface;
use Prueba\Domain\Repositories\RideServiceRepositoryInterface;
use Prueba\Domain\Repositories\VehicleTypeRepositoryInterface;
use Prueba\Infrastructure\EloquentRideServiceRepository;
use Prueba\Infrastructure\EloquentVehicleTypeRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(RideServiceRepositoryInterface::class, EloquentRideServiceRepository::class);
        $this->app->bind(CreateRideServiceApplicationInterface::class, CreateRideServiceApplication::class);
        $this->app->bind(VehicleTypeRepositoryInterface::class, EloquentVehicleTypeRepository::class);
        $this->app->bind(UpdateRideServiceApplicationInterface::class, UpdateRideServiceApplication::class);
        $this->app->bind(GetRideServiceApplicationInterface::class, GetRideServiceApplication::class);
    }
}
