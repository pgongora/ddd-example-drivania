<?php

declare(strict_types=1);

namespace Prueba\Domain\Repositories;

use App\Models\VehicleType;
use Prueba\Domain\VehicleTypeDomain;

interface VehicleTypeRepositoryInterface
{
    public function create(VehicleTypeDomain $vehicleType): VehicleTypeDomain;

    public function find(string $vehicleTypeId): VehicleTypeDomain;

    public static function toDomain(VehicleType $model): VehicleTypeDomain;
}
